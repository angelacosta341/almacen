def solve(maze, pos = [0, 0]):
    x = pos[0]
    y = pos[1]
    flag = True
    tmp = maze[x][y]
    #print(x, y)
    if tmp == 1:
        return False
    maze[x][y] = "X"
    if y > 0 and maze[x][y-1] != "X":
        flag = solve(maze, [x, y-1])
    if y < len(maze[0])-1 and maze[x][y+1] != "X":
        flag = solve(maze, [x, y+1])
    if x > 0 and maze[x-1][y] != "X":
        # Comprueba arriba
        flag = solve(maze, [x-1, y])
    if x < len(maze)-1 and maze[x+1][y] != "X":
        flag = solve(maze, [x+1, y])

    maze[x][y] = tmp
    #for i in maze:
    #    print(i)
    #print("\n")
    if x == len(maze)-1 and y == len(maze[0])-1:
        print("_______________________")
        for i in maze:
            print(i)
        print("_______________________")
        return True

maze = [[0, 0, 0, 0],
        [1, 0, 1, 0],
        [1, 0, 1, 1],
        [0, 0, 0, 0]]
solve(maze)
