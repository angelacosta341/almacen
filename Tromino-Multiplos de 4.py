length = 8
hollow = [2, 5]
table = []
number = 1
for i in range(length):
    line = []
    for j in range(length):
        line.append("■" if [i, j] != hollow else "X")
    table.append(line)
size = length ** 2 - 1
pieces = size / 3
def printTable():
    global table
    print("    ", end="")
    [print(f"{i+1:2d}  ", end="") for i in range(len(table))]
    print("")
    print(f"    {"_"*len(table)*4}")
    for i in range(len(table)):
        print(f"{i+1:2d} |", end="")
        for j in range(len(table[i])):
            print(f" {table[i][j]:2s} ", end = "")
        print("")
    print("")
def start(hollow):
    global table, number
    x = hollow[0] // 2 * 2
    y = hollow[1] // 2 * 2
    fillable = 0
    for i in range(x, x+2):
        for j in range(y, y+2):
            if table[i][j] == "■":
                fillable += 1
    #print(f"{fillable} de {table}")
    if fillable != 3:
        return None
    quadrant = [x, y]
    for i in range(x, x+2):
        for j in range(y, y+2):
            #print(i, j, hollow)
            if i != hollow[0] or j != hollow[1]:
                table[i][j] = f"{number}"
                SetSectorCheck([i, j], 1)
    number += 1
    print("\n")
    printTable()
    print("\n")
    if CheckSector([x, y]):
        print("ENHORABUENA HAS TERMINADO CON EL SECTOR, PASAMOS AL SIGUIENTE", end=" - ")
        if len(table) > x+2 and len(table[x]) > y+2:
            print("AbD")
            connection([x+1, y+1], [1, 1])
        if len(table) > x+2 and y > 0:
            print("AbI")
            connection([x+1, y], [1, -1])
        if x > 0 and y > 0:
            print("ArD")
            connection([x, y], [-1, -1])
        if x > 0 and len(table[x]) > y+2:
            print("ArI")
            connection([x, y+1], [-1, 1])
        sector = GetSector([x, y])
        sector[0] *= 4
        sector[1] *= 4
        if sector[0] > 0 and sector[1] > 0:
            connection([sector[0], sector[1]], [-1, -1])
        if sector[0] > 0 and len(table[sector[0]]) > sector[1]+4:
            connection([sector[0], sector[1]+3], [-1, 1])
        if len(table) > sector[0]+4 and y > 0:
            connection([sector[0]+3, sector[1]], [1, -1])
        if len(table) > sector[0]+4 and len(table[sector[0]]) > sector[1]+4:
            connection([sector[0]+3, sector[1]+3], [1, 1])
    else:
        sector = GetSector([x, y])
        if len(table) > x+2 and len(table[x]) > y+2 and sector == GetSector([x+2, y+2]):
            connection([x+1, y+1], [1, 1])
        if len(table) > x+2 and y > 0 and sector == GetSector([x+2, y-1]):
            connection([x+1, y], [1, -1])
        if x > 0 and y > 0 and sector == GetSector([x-1, y-1]):
            connection([x, y], [-1, -1])
        if x > 0 and len(table[x]) > y+2 and sector == GetSector([x-1, y+1]):
            connection([x, y+1], [-1, 1])
        
def connection(hollow, direction):
    global table, number
    fillable = 0
    x = hollow[0] + (0 if direction[0]>0 else direction[0])
    y = hollow[1] + (0 if direction[1]>0 else direction[1])
    #print(f"{[x, y]} en {[hollow]} direccion {[direction]}")
    for i in range(x, x+2):
        for j in range(y, y+2):
            if table[i][j] == "■":
                fillable += 1
    if fillable != 3:
        #print(f"\nHEY, AQUI TE DEJASTE ALGO {hollow} EN DIRECCION {direction}.\nTENIAS POR RELLENAR {fillable}\n")
        return None
    for i in range(x, x+2):
        for j in range(y, y+2):
            if table[i][j] == "■":
                table[i][j] = f"{number}"
                SetSectorCheck([i, j], 1)
    number += 1
    if not CheckSector(hollow):
        if GetSector([hollow[0]+direction[0], hollow[1]+direction[1]]) == GetSector(hollow):
            start([hollow[0]+direction[0], hollow[1]+direction[1]])
        if GetSector([hollow[0], hollow[1]+direction[1]]) == GetSector(hollow):
            start([hollow[0], hollow[1]+direction[1]])
        if GetSector([hollow[0]+direction[0], hollow[1]]) == GetSector(hollow):
            start([hollow[0]+direction[0], hollow[1]])
    else:
        start([hollow[0]+direction[0], hollow[1]+direction[1]])
        start([hollow[0], hollow[1]+direction[1]])
        start([hollow[0]+direction[0], hollow[1]])

def GetSector(position):
    return [position[0] // 4, position[1] // 4]
def GetSectorCheck(position):
    global sectorsCheck
    sector = GetSector(position)
    x = position[0] - 4 * sector[0]
    y = position[1] - 4 * sector[1]
    return sectorsCheck[sector[0]][sector[1]][x][y]
def SetSectorCheck(position, value):
    global sectorsCheck
    sector = GetSector(position)
    x = position[0] - 4 * sector[0]
    y = position[1] - 4 * sector[1]
    sectorsCheck[sector[0]][sector[1]][x][y] = value
def CheckSector(position):
    global sectorsCheck
    sector = GetSector(position)
    check = True
    for i in range(0, len(sectorsCheck[sector[0]][sector[1]])):
        print(f"LINEA {i}")
        for j in range(0, len(sectorsCheck[sector[0]][sector[1]][i])):
            print(f"POSICION {sector[0]},{sector[1]},{i},{j} IS {sectorsCheck[sector[0]][sector[1]][i][j]}")
            if sectorsCheck[sector[0]][sector[1]][i][j] != 1:
                print(f"POSICION {sector[0]},{sector[1]},{i},{j} IS FALSE")
                check = False
                return check
    return check
def PrintSectors():
    global sectorsCheck
    print("__________________________")
    for i in range(len(sectorsCheck)):
        print("")
        for j in range(0, len(sectorsCheck[i]), 2):
            print("", end="")
            for k in range(0, len(sectorsCheck[i][j])):
                for l in range(len(sectorsCheck[i][j][k])):
                    print(f"{sectorsCheck[i][j][k][l]:2d}", end="")
                print("  ", end="")
                for l in range(len(sectorsCheck[i][j][k])):
                    print(f"{sectorsCheck[i][j+1][k][l]:2d}", end="")
                print("")
                    
    print("__________________________")
                    
sectorX = len(table) // 4
sectorY = len(table[0]) // 4
sectorsCheck = []
for i in range(sectorX):
    line = []
    for j in range(sectorY):
        sector = []
        for k in range(4):
            sLine = []
            for l in range(4):
                sLine.append(0)
            sector.append(sLine)
        line.append(sector)
    sectorsCheck.append(line)
SetSectorCheck(hollow, 1)
PrintSectors()
startX = 1 if hollow[0] % 2 == 0 else -1
startY = 1 if hollow[1] % 2 == 0 else -1
#connection(hollow, [startX, startY])
start(hollow)
print("_____________________________________________________\n\n")
printTable()